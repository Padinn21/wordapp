package com.example.wordapp.data

data class WordData (
    val id: Char,
    val word: ArrayList<String>
        )