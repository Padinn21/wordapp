package com.example.wordapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.wordapp.data.WordData


class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    val wordList = arrayListOf(
        WordData('A', arrayListOf("Ayam","Api","Air")),
        WordData('B', arrayListOf("Banjir","Bakar","Berenang")),
        WordData('C', arrayListOf("Cair","Cepat","Cuka")),
        WordData('D', arrayListOf("Dodol","Dongkrak","Dolar")),
        WordData('E', arrayListOf("Ekor","Eskrim","Elang")),
        WordData('F', arrayListOf("Full","Fakir","Fungsi")),
        WordData('G', arrayListOf("Ganjil","Goreng","Gajah")),
        WordData('H', arrayListOf("Hantu","Hidup","Hilang")),
        WordData('I', arrayListOf("Intan","Indah","Ikan")),
        WordData('J', arrayListOf("Jaring","Jumlah","Jagung"))
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_ctr) as NavHostFragment

            navController = navHostFragment.navController
            setupActionBarWithNavController(navController)

}

    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp() || super.onSupportNavigateUp()
    }
}