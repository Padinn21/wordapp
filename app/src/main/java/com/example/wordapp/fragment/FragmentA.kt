package com.example.wordapp.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordapp.MainActivity
import com.example.wordapp.adapter.DiffAdapter
import com.example.wordapp.databinding.FragmentABinding
import java.util.*

class FragmentA : Fragment() {
    private var _binding: FragmentABinding? = null
    private lateinit var adapter:  DiffAdapter
    private val binding get() =_binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentABinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = DiffAdapter()
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvPertama.layoutManager = layoutManager

        binding.rvPertama.adapter = adapter
        adapter.submitData(MainActivity().wordList)

        adapter.setOnItemClickListener(object : DiffAdapter.onItemClickListener{
            override fun onItemClickListener(position: Int) {

            }

        })


    }

}