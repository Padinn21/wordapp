package com.example.wordapp.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.wordapp.R
import com.example.wordapp.data.WordData
import com.example.wordapp.databinding.ButtonLayoutBinding

class DiffAdapter : RecyclerView.Adapter<DiffAdapter.ViewHolder>(){
    private lateinit var mlistener: onItemClickListener


    interface onItemClickListener{
        fun onItemClickListener(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mlistener = listener
    }

    private val diffCallback = object : DiffUtil.ItemCallback<WordData>(){
        override fun areItemsTheSame(oldItem: WordData, newItem: WordData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: WordData, newItem: WordData): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    fun submitData (value: ArrayList<WordData>) = differ.submitList(value)

    override fun onCreateViewHolder(parent:ViewGroup, viewType: Int) : ViewHolder {
       val binding =
           ButtonLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, mlistener)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = differ.currentList[position]
        val mbundle = Bundle()
        mbundle.putChar("KEY_CHAR", data.id)
        mbundle.putStringArrayList("KEY_WORD", data.word)
        with(holder) {
            binding.btnItem.text = data.id.toString()
            binding.btnItem.setOnClickListener {
                itemView.findNavController().navigate(R.id.action_fragmentA_to_fragmentB, mbundle)
            }
        }

    }

    override fun getItemCount(): Int  = differ.currentList.size

    class ViewHolder(val binding: ButtonLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {
        init {
            listener.onItemClickListener(adapterPosition)
        }

        }


}