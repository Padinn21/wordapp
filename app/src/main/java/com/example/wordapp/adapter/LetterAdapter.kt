package com.example.wordapp.adapter

import android.app.SearchManager
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.example.wordapp.R

class LetterAdapter(
    val list: ArrayList<String>
    ): RecyclerView.Adapter<LetterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.button_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val word = list[position]

        with(holder) {
            button.text = word
            button.setOnClickListener {
                val intent = Intent(Intent.ACTION_WEB_SEARCH)
                val url = "https://www.google.com/search?q=${word}"
                intent.putExtra(SearchManager.QUERY, url)
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val button: Button = itemView.findViewById(R.id.btn_item)
    }


}